# Doxygen integration
## Usage

The configuration possibilities are limited because otherwise the configuration of this would have too many entrys to be filled out.

1.	`Makefile.am` variables:
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~make
	# Provide a list of source files for Doxygen:
	DOXYGEN_DOCSOURCES  = $(mylib_SOURCES) $(mybin_SOURCES)
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
2.	Include the module in `Makefile.am` (adjust the path):
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~make
	include templates/doxygen/doxygen.mk
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	Make sure the `CLEANFILES` variable is initialized before.
3.	Include the module in `configure.ac` (again, adjust the path):
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~m4
	AC_CONFIG_MACRO_DIRS([templates/doxygen/m4])
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
4.	Call the relevant macro with a directory to use for documentation related things and the path to this module:
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~m4
	AX_TEMPLATES_DOXYGEN([doc], [templates/doxygen])
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
5.	One file will have to be generated. It lists all documentation files. Include it in `Makefile.am` (the first path you specified at `AX_TEMPLATES_DOXYGEN` with `/doc.mk`):
	~~~~~~~~~~~~~~~~make
	include doc/doc.mk
	~~~~~~~~~~~~~~~~
	Before every commit, update it. The target is called like the directory with `-update` appended. In this case it would be called `doc-update`.
6.	Set Makefile.am DOXYGEN\_PROJECT\_NUMBER to something the shell substitutes by the revision of the project.

## How does this work?

Most of the magic is done in the call to `AX_TEMPLATES_DOXYGEN`.
It adds the following options to the `./configure` script:
- `--enable-doc`
- `--enable-doc-html`
- `--enable-doc-man`
- `--enable-doc-warn`
- `--enable-doc-internal`
- `--with-doxygen`
It substitutes a lot:
~~~~~~~~~~~~~~~~~~~~~~~~~~~m4
AC_SUBST(DOXYGEN)
AC_SUBST(DOXYGEN_DOCPATH)
AC_SUBST(DOXYGEN_MODPATH)
AM_CONDITIONAL(ENABLE_DOC_ALL_AM)
AM_CONDITIONAL(DISABLE_DOC_AM)
AM_CONDITIONAL(ENABLE_DOC_HTML_AM)
AM_CONDITIONAL(ENABLE_DOC_MAN_AM)
~~~~~~~~~~~~~~~~~~~~~~~~~~~
It also adds some output files:
~~~
AC_CONFIG_FILES([$DOXYGEN_DOCPATH/Doxytmp:$DOXYGEN_DOCPATH/Doxyfile.in:$DOXYGEN_MODPATH/Doxyfile.in])
~~~
