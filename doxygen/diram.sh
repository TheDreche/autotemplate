#!/bin/sh

export IFS=""

self="$0"

isn() {
	return "$1"
}

is() {
	if isn "$1"; then
		return 1
	else
		return 0
	fi
}

fail() {
	printf '%s\n' "$2" >&2
	exit "$1"
}

# Fixed outputs
msg_help() {
	cat << EOH
Usage: $self <options> -- <tools> <filelists>

Options include:
  --help, -h      Print this help message
  --verbose, -v   Be more verbose
  --head          Head to prepend to every file
  --tail          Tail to append to every file

Tools example:
  HEAD /bin/head SED /bin/sed

Tools needed:
  HEAD
  SED

Filelists have a special syntax. An example:
  $self FOLDER doc/html AT doc/html AS DATA TO doc , FOLDER doc/man/man3 AT doc/man/man3 AS MANS TO man3 INTO doc.mk

The above command would generate doc.mk to be a file to be included into Makefile.am for installing doc/html to htmldir and doc/man/man3 into man3dir.

If the directory structure would be the following, you could also write what is below the following into doc.mk
- doc/
  - html/
    - subhtml/
      - subindex.html
      - substyle.css
    - index.html
    - style.css
  - man/
    - man3/
      - mybinary.1
      - libother.3

  doc_DATA = \
  	doc/html/index.html \
  	doc/html/style.css
  
  _subhtml_docdir = \$(docdir)/subhtml
  _subhtml_doc_DATA = \
  	doc/html/subhtml/subindex.html \
  	doc/html/subhtml/substyle.css
  
  man3_MANS = \
  	doc/man/man3/mybinary.1 \
  	doc/man/man3/libother.3

Each folder you want to install consists out of the following:
  FOLDER - Path to the folder for Makefile
  AT - Path to the folder for this sript
  AS - The type of data to be installed. For example, DATA or MANS
  IF - (Optonal) Only install if following automake conditional is true
  VAR - (Optonal) A Makefile variable all files should be added to
  TO - The folder this should get installed in, without dir.
You can add many of such separated by space comma space. Then, an INTO should come followed by a space-separated list of files this should be written to. Then, a comma should follow if more files should be converted.
EOH
}

# Option declarations
# The following is a newline-separated list. It contains the keywords followed directly by their value. Output files are all prefixed with the keyword INTO.
opt_file_converts=""
opt_verbose=0

# Following will originally save a file name and then will save the file content:
opt_head=''
opt_tail=''

tool_head=""
tool_sed=""

# Functions for parsing options
_opt_file_converts_add_convadd() {
	# Dealing with such lists is complicated.
	# This function has opt_file_converts as stdin
	# and will add a conversion.
	from_folder="$1"
	from_at="$2"
	from_as="$3"
	from_to="$4"
	output="$5"
	from_if="$6"
	from_var="$7"
	found_inserted=0
	found_insert=0
	found_reset=0
	found_folder=0
	found_at=0
	found_as=0
	found_to=0
	found_if=0
	found_var=0
	while read -r line; do
		if is "$found_inserted"; then
			printf '%s\n' "$line"
		else
			# First search whether the input file will be converted already.
			case "$line" in
				FOLDER*)
					if is "$found_folder"; then
						found_insert=1
					elif test "$line" = "FOLDER$from_folder"; then
						found_folder=1
					else
						found_reset=1
					fi;;
				AT*)
					if is "$found_at"; then
						found_insert=1
					elif test "$line" = "AT$from_at"; then
						found_at=1
					else
						found_reset=1
					fi;;
				AS*)
					if is "$found_as"; then
						found_insert=1
					elif test "$line" = "AS$from_as"; then
						found_as=1
					else
						found_reset=1
					fi;;
				IF*)
					if is "$found_if"; then
						found_insert=1
					elif test "$line" = "IF$from_if"; then
						found_if=1
					else
						found_reset=1
					fi;;
				VAR*)
					if is "$found_var"; then
						found_insert=1
					elif test "$line" = "VAR$from_var"; then
						found_var=1
					else
						found_reset=1
					fi;;
				TO*)
					if is "$found_to"; then
						found_insert=1
					elif test "$line" = "TO$from_at"; then
						found_to=1
					else
						found_reset=1
					fi;;
			esac
			if is "$found_insert"; then
				found_insert=0
				if is "$found_folder" && is "$found_at" && is "$found_as" && is "$found_if" && is "$found_to" && is "$found_var"; then
					printf 'INTO%s\n' "$output"
					found_inserted=1
				else
					found_reset=1
				fi
			fi
			if is "$found_reset"; then
				found_reset=0
				found_folder=0
				found_at=0
				found_as=0
				found_if=0
				found_var=0
				found_to=0
			fi
			printf '%s\n' "$line"
		fi
	done
	if isn "$found_inserted"; then
		# Else insert it
		printf 'FOLDER%s\n' "$from_folder"
		printf 'AT%s\n' "$from_at"
		printf 'AS%s\n' "$from_as"
		printf 'IF%s\n' "$from_if"
		printf 'VAR%s\n' "$from_var"
		printf 'TO%s\n' "$from_to"
		printf 'INTO%s\n' "$output"
	fi
}

opt_file_converts_add() {
	from_folder="$1"
	from_at="$2"
	from_as="$3"
	from_to="$4"
	output="$5"
	from_if="$6"
	from_var="$7"
	tmp="$opt_file_converts"
	opt_file_converts="$(printf '%s\n' "$tmp" | _opt_file_converts_add_convadd "$from_folder" "$from_at" "$from_as" "$from_to" "$output" "$from_if" "$from_var")"
}

# Complex parsing operations
queue_in_converts=""

# Same as above (into is not saved)
_queue_in_converts_add_convadd() {
	from_folder="$1"
	from_at="$2"
	from_as="$3"
	from_to="$4"
	from_if="$5"
	from_var="$6"
	found_inserted=0
	found_insert=0
	found_reset=0
	found_folder=0
	found_at=0
	found_as=0
	found_to=0
	found_if=0
	found_var=0
	while read -r line; do
		if is "$found_inserted"; then
			printf '%s\n' "$line"
		else
			case "$line" in
				FOLDER*)
					if is "$found_folder"; then
						found_insert=1
					elif test "$line" = "FOLDER$from_folder"; then
						found_folder=1
					else
						found_reset=1
					fi;;
				AT*)
					if is "$found_at"; then
						found_insert=1
					elif test "$line" = "AT$from_at"; then
						found_at=1
					else
						found_reset=1
					fi;;
				AS*)
					if is "$found_as"; then
						found_insert=1
					elif test "$line" = "AS$from_as"; then
						found_as=1
					else
						found_reset=1
					fi;;
				IF*)
					if is "$found_if"; then
						found_insert=1
					elif test "$line" = "IF$from_if"; then
						found_if=1
					else
						found_reset=1
					fi;;
				VAR*)
					if is "$found_var"; then
						found_insert=1
					elif test "$line" = "VAR$from_if"; then
						found_var=1
					else
						found_reset=1
					fi;;
				TO*)
					if is "$found_to"; then
						found_insert=1
					elif test "$line" = "TO$from_at"; then
						found_to=1
					else
						found_reset=1
					fi;;
			esac
			if is "$found_insert"; then
				found_insert=0
				if is "$found_folder" && is "$found_at" && is "$found_as" && is "$found_if" && is "$found_to" && is "$found_var"; then
					found_inserted=1
				else
					found_reset=1
				fi
			fi
			if is "$found_reset"; then
				found_reset=0
				found_folder=0
				found_at=0
				found_as=0
				found_if=0
				found_to=0
				found_var=0
			fi
			printf '%s\n' "$line"
		fi
	done
	if isn "$found_inserted"; then
		printf 'FOLDER%s\n' "$from_folder"
		printf 'AT%s\n' "$from_at"
		printf 'AS%s\n' "$from_as"
		printf 'IF%s\n' "$from_if"
		printf 'VAR%s\n' "$from_var"
		printf 'TO%s\n' "$from_to"
	fi
}

queue_in_converts_add() {
	from_folder="$1"
	from_at="$2"
	from_as="$3"
	from_to="$4"
	from_if="$5"
	from_var="$6"
	tmp="$queue_in_converts"
	queue_in_converts="$(printf '%s\n' "$tmp" | _queue_in_converts_add_convadd "$from_folder" "$from_at" "$from_as" "$from_to" "$from_if" "$from_var")"
}

_queue_in_converts_transscript_transscript() {
	output="$1"
	from_folder=""
	from_at=""
	from_as=""
	from_if=""
	from_to=""
	from_var=""
	while read -r line; do
		case "$line" in
			FOLDER*) from_folder="${line#FOLDER}";;
			AT*) from_at="${line#AT}";;
			AS*) from_as="${line#AS}";;
			IF*) from_if="${line#IF}";;
			VAR*) from_var="${line#VAR}";;
			TO*) # Entry finished.
				from_to="${line#TO}"
				opt_file_converts_add "$from_folder" "$from_at" "$from_as" "$from_to" "$output" "$from_if" "$from_var"
				from_folder=""
				from_at=""
				from_as=""
				from_if=""
				from_to=""
				from_var=""
				;;
		esac
	done
}

queue_in_converts_transscript() {
	out="$1"
	_queue_in_converts_transscript_transscript "$out" << EOF
$queue_in_converts
EOF
}

# Actual parsing
invalid_options=""
invalid_values=""
invalid_things=""
invalid_conversions=""

parse_opt_long() {
	opt="$1"
	case "$opt" in
		--help) msg_help; exit 0;;
		--verbose=y|--verbose=yes|--verbose) opt_verbose=1;;
		--verbose=n|--verbose=no) opt_verbose=0;;
		--verbose=*) invalid_values="$invalid_values $opt";;
		--head=*) opt_head="${opt#--head=}";;
		--head) opt_head='';;
		--tail=*) opt_tail="${opt#--tail=}";;
		--tail) opt_tail='';;
		*) invalid_options="$invalid_options $opt";;
	esac
}

parse_opt_short() {
	opt="$1"
	case "$opt" in
		h) msg_help; exit 0;;
		v) opt_verbose=1;;
		*) invalid_options="$invalid_options -$opt";;
	esac
}

reading_outputs=0
parsing_thing=''
parse_key() {
	case "$1" in
		HEAD|SED) parsing_thing="~$1";;
		INTO)
			queue_in_converts_add "$current_folder" "$current_at" "$current_as" "$current_to" "$current_if" "$current_var"
			reading_outputs=1;;
		FOLDER|AT|AS|IF|VAR|TO) parsing_thing="#$1";;
		,)
			if test -n "$current_folder" && test -n "$current_at" && test -n "$current_as" && test -n "$current_to"; then
				queue_in_converts_add "$current_folder" "$current_at" "$current_as" "$current_to" "$current_if" "$current_var"
			else
				invalid_conversions="$invalid_conversions FOLDER=$current_folder AT=$current_at AS=$current_as IF=$current_if VAR=$current_var TO=$current_to ,"
			fi;;
		*) invalid_things="$invalid_things $1"
	esac
}

parse_tool() {
	case "${1#'~'}" in
		HEAD) tool_head="$2";;
		SED) tool_sed="$2";;
	esac
}

current_folder=''
current_at=''
current_as=''
current_if=''
current_var=''
current_to=''
parse_keyval() {
	if test -z "$2"; then
		invalid_things="$invalid_things ${1#'#'}=$2"
		return
	fi
	case "${1#'#'}" in
		FOLDER) current_folder="$2";;
		AT) current_at="$2";;
		AS) current_as="$2";;
		IF) current_if="$2";;
		VAR) current_var="$2";;
		TO) current_to="$2";;
	esac
}

canbeopt=1
for arg; do
	isread=0
	if test -n "$parsing_thing"; then
		case "$parsing_thing" in
			'#'*) parse_keyval "$parsing_thing" "$arg";;
			'~'*) parse_tool "$parsing_thing" "$arg";;
		esac
		parsing_thing=""
		isread=1
	elif is "$canbeopt"; then
		isread=1
		case "$arg" in
			--|-) canbeopt=0;;
			--*) parse_opt_long "$arg";;
			-*)	argcpy="${arg#-}"
				while test -n "$argcpy"; do
					parse_opt_short "$(printf "%.1s\n" "$argcpy")"
					argcpy="${argcpy#?}"
				done;;
			*) isread=0;;
		esac
	fi
	if isn "$isread"; then
		if is "$reading_outputs"; then
			if test "$arg" = ','; then
				queue_in_converts=''
				reading_outputs=0
			else
				queue_in_converts_transscript "$arg"
			fi
		else
			parse_key "$arg"
			isread=1
		fi
	fi
done

# Checking for invalid arguments
wrongargs=0
if test -n "$invalid_options"; then
	wrongargs=1
	printf 'Invalid options:\n%s\n\n' "$invalid_options"
fi
if test -n "$invalid_values"; then
	wrongargs=1
	printf 'Invalid options for arguments:\n%s\n\n' "$invalid_values"
fi
if test -n "$invalid_things"; then
	wrongargs=1
	printf 'Invalid tools & conversion args:\n%s\n\n' "$invalid_things"
fi
if test -n "$invalid_conversions"; then
	wrongargs=1
	printf 'Invalid conversions:\n%s\n\n' "$invalid_conversions"
fi
if is "$wrongargs"; then
	printf 'For help use %s --help\n' "$self"
	exit 1
fi

# Functions for getting default options
# Actually, the project root is not saved
_get_dir_project_root_is() {
	test -f configure.ac
	return $?
}

get_dir_project_root() {
	(
		if test -n "$SRCDIR"; then
			cd "$SRCDIR"
			if is "$?"; then
				if _get_dir_project_root_is; then
					pwd
					exit 0
				fi
			fi
		fi
		exit 1
	) || (
		dir_prev=""
		while ! test "$dir_prev" = "$(pwd)"; do
			if _get_dir_project_root_is; then
				pwd
				exit 0
			fi
			dir_prev="$(pwd)"
			cd ..
		done
		exit 1
	) || (
		dir_prev=""
		case "$self" in
			/) cd / || exit 1;;
			*/*/) cd "${self%/*/}" || exit 1;;
			*/*) cd "${self%/*}" || exit 1;;
		esac
		while ! test "$dir_prev" = "$(pwd)"; do
			if _get_dir_project_root_is; then
				pwd
				exit 0
			fi
			dir_prev="$(pwd)"
			cd ..
		done
		exit 1
	)
	return "$?"
}

tool() {
	which "$1"
	return "$?"
}

# Set opt_head and opt_tail to file contents
opt_head_process() {
	head="$1"
	if test -z "$head"; then
		opt_head_process '-' << EOH
#!/usr/bin/automake
# 
# This file is generated automatically!
# Changes will get lost!
# 
# Generated by $self $*
# 
# The purpose of this file is to be able
# to install full directorys using automake.
# 

EOH
	elif test '-' = "$head"; then
		while read -r line; do
			printf '%s\n' "$line"
		done
		echo 'sth' # For keeping final newlines
	else
		opt_head_process '-' < "$head"
	fi
}

opt_tail_process() {
	tail="$1"
	if test -z "$tail"; then
		opt_tail_process '-' << EOH
# vim:filetype=automake:noexpandtab:
EOH
	elif test '-' = "$tail"; then
		while read -r line; do
			printf '%s\n' "$line"
		done
		echo 'sth' # For keeping final newlines
	else
		opt_tail_process '-' < "$tail"
	fi
}

# Setting default options
if test -z "$tool_head"; then
	tool_head="$(tool head)"
fi
if test -z "$tool_sed"; then
	tool_sed="$(tool sed)"
fi
# The final newline is removed later by leaving it out in printf format
opt_head="$(opt_head_process "$opt_head")"
opt_head="${opt_head%sth}"
opt_tail="$(opt_tail_process "$opt_tail")"
opt_tail="${opt_tail%sth}"

# Action!

# Stdin:
# Ffile1
# Ffile2
# ---
# line1
# line2
# 
# Writes the lines into multiple files
multify_to_files() {
	read -r file || return
	if test "$file" = '---'; then
		while read -r line; do
			if test "$line" = ' '; then
				line=''
			fi
			for f; do
				if test '-' = "$f"; then
					printf '%s\n' "$line"
				else
					printf '%s\n' "$line" >> "$f"
				fi
			done
		done
	elif test -n "$file"; then
		multify_to_files "$@" "${file#F}"
	else
		multify_to_files "$@"
	fi
}

# Override every file with the head
# The content will just be appended
_prepare_files() {
	while read -r line; do
		case "$line" in
			INTO*)
				file="${line#INTO}"
				if test '-' = "$file"; then
					printf '%s' "$opt_head"
				else
					printf '%s' "$opt_head" > "$file"
				fi;;
		esac
	done
}

prepare_files() {
	printf '%s\n' "$opt_file_converts" | _prepare_files
}

prepare_files

#
# Most important functions:
# The functions doing the actual conversion.
# The definitions follow.
#

# Echo value for single directory
# Currently not sorted
_convert_ls() {
	arg_folder="$1"
	arg_at="$2"
	arg_as="$3"
	arg_to="$4"
	arg_var="$5"
	if test -z "$arg_var"; then
		arg_var=CLEANFILES
	fi
	
	(
		printf "$arg_var += \$(nodist_%s_%s)\n" "$arg_to" "$arg_as"
		printf 'nodist_%s_%s = \\\n' "$arg_to" "$arg_as"
		for f in *; do
			if test -f "$f"; then
				printf '\t%s/%s \\\n' "$arg_folder" "$f"
			fi
		done
	) | "$tool_head" -c-3
	printf '\n'
}

# Convert any directory path to a Makefile variable name
_convert_dirtomakevarname() {
	printf '%s\n' "$1" | "$tool_sed" 's/[^a-zA-Z0-9]/_/g'
}

wd_before="$(pwd)"
# Recursively list all files in directory
_convert_recurse() {
	arg_folder="$1"
	arg_at="$2"
	arg_as="$3"
	arg_to="$4"
	arg_var="$5"
	
	if is "$opt_verbose"; then
		printf "Searching for files in %s\n" "$arg_folder" >&3
	fi
	
	cd "$arg_at" || return
	
	_convert_ls "$arg_folder" "$arg_at" "$arg_as" "$arg_to" "$arg_var" || return
	
	arg_folder="$1"
	arg_at="$2"
	arg_as="$3"
	arg_to="$4"
	arg_var="$5"
	
	printf '\n'
	
	for d in *; do
		if test -d "$d"; then
			cd "$wd_before" || return
			printf "%s = \$(%sdir)/%s\n" "$(_convert_dirtomakevarname "_${arg_to}_${d}dir")" "$arg_to" "$d"
			_convert_recurse "$arg_folder/$d" "$arg_at/$d" "$arg_as" "$(_convert_dirtomakevarname "_${arg_to}_${d}")" "$arg_var"
			arg_folder="$1"
			arg_at="$2"
			arg_as="$3"
			arg_to="$4"
			arg_var="$5"
			cd "$arg_at" || return
		fi
	done
	cd "$wd_before" || return
}

# Add if to recursive list
convert() {
	arg_folder="$1"
	arg_at="$2"
	arg_as="$3"
	arg_if="$4"
	arg_to="$5"
	arg_var="$6"
	
	if test -n "$arg_if"; then
		printf 'if %s\n\n' "$arg_if"
	fi
	_convert_recurse "${arg_folder%/}" "${arg_at%/}" "$arg_as" "$arg_to" "$arg_var"
	
	arg_folder="$1"
	arg_at="$2"
	arg_as="$3"
	arg_if="$4"
	arg_to="$5"
	arg_var="$6"
	
	if test -n "$arg_if"; then
		printf 'else\nnodist_%s_%s = \nendif\n' "$arg_to" "$arg_as"
	fi
	printf ' \n'
}

# Reusing variables from above
current_folder=''
current_at=''
current_as=''
current_if=''
current_var=''
current_to=''

# Not reused
current_intos=''

set_var() {
	case "$1" in
		FOLDER*) current_folder="${1#FOLDER}";;
		AT*) current_at="${1#AT}";;
		AS*) current_as="${1#AS}";;
		IF*) current_if="${1#IF}";;
		VAR*) current_var="${1#VAR}";;
		TO*) current_to="${1#TO}";;
	esac
}

# Functions for adding to the list of files a conversion should go to
_into_add_add() {
	while read -r line; do
		printf '%s\n' "$line"
	done
	for l; do
		printf 'F%s\n' "$l"
	done
}

into_add() {
	tmp="$current_intos"
	current_intos="$(printf '%s\n' "$tmp" | _into_add_add "$@")"
}

# Do actual conversion
_process_actions_convert() {
	(
		multify_to_files << EOF
$current_intos
---
$(convert "$current_folder" "$current_at" "$current_as" "$current_if" "$current_to" "$current_var")
EOF
	) 3>&1
}

# Read the lists and do the conversion
_process_actions() {
	# stdin: $opt_file_converts
	parsing_intos=0
	while read -r line; do
		case "$line" in
			INTO*) parsing_intos=1; into_add "${line#INTO}";;
			*)
				if is "$parsing_intos"; then
					parsing_intos=0
					_process_actions_convert
					current_intos=''
				fi
				set_var "$line";;
		esac
	done
	if is "$parsing_intos"; then
		parsing_intos=0
		_process_actions_convert
		current_intos=''
	fi
}

process_actions() {
	printf '%s\n' "$opt_file_converts" | _process_actions
}

process_actions

# Append tail to all files

output_with_tail=''

_append_tail_file() {
	# stdin: "$output_with_tail"
	added=0
	while read -r line; do
		echo "$line"
		case "$line" in
			F"$1") added=1;;
		esac
	done
	if isn "$added"; then
		case "$1" in
			-) echo "$opt_tail" >&3;;
			*) echo "$opt_tail" >> "$1";;
		esac
		echo "F$1"
	fi
}

_append_tail() {
	while read -r line; do
		case "$line" in
			INTO*)
				file="${line#INTO}"
				output_with_tail="$(echo "$output_with_tail" | _append_tail_file "$file")" 3>&1;;
		esac
	done
}

append_tail() {
	printf '%s\n' "$opt_file_converts" | _append_tail
}

append_tail
