#!/usr/bin/automake

doxygen_verbose = $(doxygen_verbose_@AM_V@)
doxygen_verbose_ = $(doxygen_verbose_@AM_DEFAULT_V@)
doxygen_verbose_0 = @echo "  DOXYGEN " $@;

$(DOXYGEN_DOCPATH)/Doxyfile : $(DOXYGEN_DOCPATH)/Doxytmp
	@DOXYGEN_DOCSOURCES_=''; \
		for f in $(DOXYGEN_DOCSOURCES); do \
			DOXYGEN_DOCSOURCES_="$$DOXYGEN_DOCSOURCES_ $(top_srcdir)/$$f"; \
		done; \
		$(SED) "s:~top_srcdir~:"$(top_srcdir)":g;s|~PROJECT_NUMBER~|"$(DOXYGEN_PROJECT_NUMBER)"|g;s'~SOURCES~'$${DOXYGEN_DOCSOURCES_}'g" $< > $@

CLEANFILES += $(DOXYGEN_DOCPATH)/Doxyfile $(DOXYGEN_DOCPATH)/Doxywarn

$(DOXYGEN_DOCPATH)/Doxywarn : $(DOXYGEN_DOCPATH)/Doxyfile $(DOXYGEN_DOCSOURCES)
	@$(MKDIR_P) -- $(DOXYGEN_DOCPATH)
	@touch -- $(DOXYGEN_DOCPATH)/Doxywarn
	@echo '>>> Regenerating documentation' >> $(DOXYGEN_DOCPATH)/Doxywarn
	$(doxygen_verbose) $(DOXYGEN) $(DOXYGEN_DOCPATH)/Doxyfile
if DOXYGEN_ENABLE_DOC_MAN_AM
	@for f in $(DOXYGEN_DOCPATH)/man/man3/*; do \
		if test "$$(echo "$$f" | $(SED) 's/[^a-zA-Z0-9_.-/]/_/g')" '!=' "$$f"; then \
			$(am__mv) "$$f" "$$(echo "$$f" | $(SED) 's/[^a-zA-Z0-9_.-/]/_/g')"; \
		fi; \
	done
endif
	@echo '>>> Done' >> $(DOXYGEN_DOCPATH)/Doxywarn

$(DOXYGEN_DOCFILES) : $(DOXYGEN_DOCPATH)/Doxywarn

clean-local-doxygen-man :
	rm -rf -- $(DOXYGEN_DOCPATH)/man

clean-local-doxygen-html :
	rm -rf -- $(DOXYGEN_DOCPATH)/html

clean-local : clean-local-doxygen-man clean-local-doxygen-html

if DOXYGEN_ENABLE_DOC_ALL_AM
.PHONY : $(DOXYGEN_DOCPATH)-update
$(DOXYGEN_DOCPATH)-update : $(DOXYGEN_MODPATH)/diram.sh $(DOXYGEN_DOCPATH)/Doxyfile
	@-rm -rf -- $(DOXYGEN_DOCPATH)/man
	@-rm -rf -- $(DOXYGEN_DOCPATH)/html
	@touch -- $(DOXYGEN_DOCPATH)/Doxyfile.in
	$(MAKE) $(AM_MAKEFLAGS) $(DOXYGEN_DOCPATH)/Doxyfile
	$(MAKE) $(AM_MAKEFLAGS) $(DOXYGEN_DOCPATH)/Doxywarn
	$(shell_verbose)$(SHELL) $(DOXYGEN_MODPATH)/diram.sh FOLDER $(DOXYGEN_DOCPATH)/html AT $(DOXYGEN_DOCPATH)/html IF DOXYGEN_ENABLE_DOC_HTML_AM AS DATA TO html VAR DOXYGEN_DOCFILES ',' FOLDER $(DOXYGEN_DOCPATH)/man/man3 AT $(DOXYGEN_DOCPATH)/man/man3 IF DOXYGEN_ENABLE_DOC_MAN_AM AS MANS TO man3 VAR DOXYGEN_DOCFILES INTO $(DOXYGEN_DOCPATH)/doc.mk
endif

DOXYGEN_DOCFILES =

# vim:filetype=automake:
