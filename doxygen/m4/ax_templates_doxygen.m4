# SYNOPSIS
#
#   AX_TEMPLATE_DOXYGEN(directory, path to self)
#
# DESCRIPTION
#
#   Sets up a project for using doxygen.
#   
#   directory is a directory which can contain everything related to documentation.
#   path to self is the path to the directory containing diram.sh and Doxyfile.in.

AC_DEFUN([AX_TEMPLATE_DOXYGEN], [
	dnl TODO: AC_PREREQ
	AC_CONFIG_FILES([$1/Doxytmp:$1/Doxyfile.in:$2/Doxyfile.in])
	
	AC_SUBST(DOXYGEN_ENABLE_DOC_WARN)
	AC_SUBST(DOXYGEN_ENABLE_DOC_INTERNAL)
	AC_SUBST(DOXYGEN_ENABLE_DOC_ALL)
	AC_SUBST(DOXYGEN_ENABLE_DOC_ANY)
	AC_SUBST(DOXYGEN_ENABLE_DOC_HTML)
	AC_SUBST(DOXYGEN_ENABLE_DOC_MAN)
	AC_SUBST(DOXYGEN_WITH_DOC_DOT)
	AC_SUBST(DOXYGEN_WITH_DOC_DOT_PATH)
	AC_SUBST(DOXYGEN_USE_DOT_MULTI_TARGETS)
	AC_SUBST(DOXYGEN_DOCPATH)
	AC_SUBST(DOXYGEN_MODPATH)
	DOXYGEN_DOCPATH='$1'
	DOXYGEN_MODPATH='$2'
	dnl The following gets overridden if one documentation is disabled.
	DOXYGEN_ENABLE_DOC_ALL=YES
	
	dnl | Documentation
	dnl +---------------
	dnl | Tools usable by doxygen
	DOXYGEN_WITH_DOC_DOT=NO
	DOXYGEN_WITH_DOC_DOT_PATH=''
	AC_ARG_WITH(
		[doc-dot],
		[AS_HELP_STRING([--with-doc-dot], [use dot for generating graphs in documentation @<:@default=check@:>@])],
	[
		AS_CASE(["$withval"],
			[yes], [
				DOXYGEN_WITH_DOC_DOT=YES
				DOXYGEN_have_doc_dot=yes
			], [check], [
				DOXYGEN_WITH_DOC_DOT=CHECK
			], [no], [
				DOXYGEN_WITH_DOC_DOT=NO
				DOXYGEN_have_doc_dot=no
			], [
				DOXYGEN_WITH_DOC_DOT=YES
				DOXYGEN_have_doc_dot=yes
				DOXYGEN_WITH_DOC_DOT_PATH="$withval"
			]
		)
	], [
		DOXYGEN_WITH_DOC_DOT=CHECK
	])
	AS_IF([test "x$DOXYGEN_WITH_DOC_DOT" = 'xCHECK'], [
		AC_CHECK_PROG([DOXYGEN_have_doc_dot], [dot], [yes], [no], [])
		AS_IF([test "x$DOXYGEN_have_doc_dot" = "xyes"], [
			DOXYGEN_WITH_DOC_DOT=YES
		], [
			DOXYGEN_WITH_DOC_DOT=NO
		])
	])
	DOXYGEN_USE_DOT_MULTI_TARGETS=NO
	AS_IF([test "x$DOXYGEN_WITH_DOC_DOT" = "xYES"], [
		dnl TODO: Check whether dot can handle multiple outputs at once
		dnl AC_CACHE_CHECK([whether dot can process multiple outputs ...], [ax_cv_doxygen_dot_multitarget], [])
	])
	
	dnl | Doxygen features
	AC_ARG_ENABLE(
		[doc-warn],
		[AS_HELP_STRING([--enable-doc-warn], [enable warnings from doxygen @<:@default=no@:>@])],
	[
		AS_CASE(["$enableval"],
			[yes], [
				DOXYGEN_ENABLE_DOC_WARN=YES
			], [no], [
				DOXYGEN_ENABLE_DOC_WARN=NO
			], [AC_MSG_ERROR([bad value $enableval for --enable-doc-warn])]
		)
	], [
		DOXYGEN_ENABLE_DOC_WARN=NO
	])
	
	dnl | Not matching anything else
	AC_ARG_ENABLE(
		[doc-internal],
		[AS_HELP_STRING([--enable-doc-internal], [also extract internal documentation@<:@default=no@:>@])],
	[
		AS_CASE(["$enableval"],
			[yes], [
				DOXYGEN_ENABLE_DOC_INTERNAL=YES
			], [no], [
				DOXYGEN_ENABLE_DOC_INTERNAL=NO
			], [AC_MSG_ERROR([bad value $enableval for --enable-doc-internal])]
		)
	], [
		DOXYGEN_ENABLE_DOC_INTERNAL=NO
	])
	
	dnl | Documentation file types
	DOXYGEN_ENABLE_DOC_ANY=NO
	AC_ARG_ENABLE([doc],
		[AS_HELP_STRING([--enable-doc], [enable documentation @<:@default=check@:>@])],
	[
		AS_CASE(["$enableval"],
			[yes], [
				DOXYGEN_enable_doc=yes
				DOXYGEN_ENABLE_DOC=YES
			], [check], [
				DOXYGEN_enable_doc=check
				DOXYGEN_ENABLE_DOC=CHECK
			], [no], [
				DOXYGEN_enable_doc=no
				DOXYGEN_ENABLE_DOC=NO
			], [AC_MSG_ERROR([bad value $enableval for --enable-doc}])]
		)
	], [
		DOXYGEN_enable_doc=check
		DOXYGEN_ENABLE_DOC=CHECK
	])
	AC_ARG_ENABLE([doc-html],
		[AS_HELP_STRING([--disable-doc-html], [disable HTML documentation @<:@default=same as --enable-doc@:>@])],
	[
		AS_CASE(["$enableval"],
			[yes], [
				DOXYGEN_enable_doc_html=yes
				DOXYGEN_ENABLE_DOC_HTML=YES
				DOXYGEN_ENABLE_DOC_ANY=YES
			], [check], [
				DOXYGEN_enable_doc_html=check
				DOXYGEN_ENABLE_DOC_HTML=CHECK
				AS_IF([test ENABLE_DOC_ANY = NO], [DOXYGEN_ENABLE_DOC_ANY=CHECK])
			], [no], [
				DOXYGEN_enable_doc_html=no
				DOXYGEN_ENABLE_DOC_HTML=NO
			], [AC_MSG_ERROR([bad value $enableval for --enable-doc-html])]
		)
	], [
		DOXYGEN_enable_doc_html=${enable_doc}
		DOXYGEN_ENABLE_DOC_HTML=${ENABLE_DOC}
		AS_CASE(["$DOXYGEN_enable_doc"],
			[yes], [
				DOXYGEN_ENABLE_DOC_ANY=YES
			], [check], [
				AS_IF([test DOXYGEN_ENABLE_DOC_ANY = NO], [DOXYGEN_ENABLE_DOC_ANY=CHECK])
			]
		)
	])
	AC_ARG_ENABLE(
		[doc-man],
		[AS_HELP_STRING([--disable-doc-man], [disable man pages documentation @<:@default=same as --enable-doc@:>@])],
	[
		AS_CASE(["$enableval"],
			[yes], [
				DOXYGEN_enable_doc_man=yes
				DOXYGEN_ENABLE_DOC_MAN=YES
				DOXYGEN_ENABLE_DOC_ANY=YES
			], [check], [
				DOXYGEN_enable_doc_man=check
				DOXYGEN_ENABLE_DOC_MAN=CHECK
				AS_IF([test DOXYGEN_ENABLE_DOC_ANY = NO], [DOXYGEN_ENABLE_DOC_ANY=CHECK])
			], [no], [
				DOXYGEN_enable_doc_man=no
				DOXYGEN_ENABLE_DOC_MAN=NO
			], [AC_MSG_ERROR([bad value $enableval for --enable-doc-man])]
		)
	], [
		DOXYGEN_enable_doc_man=${enable_doc}
		DOXYGEN_ENABLE_DOC_MAN=${ENABLE_DOC}
		AS_CASE(["$enable_doc"],
			[yes], [
				DOXYGEN_ENABLE_DOC_ANY=YES
			], [check], [
				AS_IF([test DOXYGEN_ENABLE_DOC_ANY = NO], [DOXYGEN_ENABLE_DOC_ANY=CHECK])
			]
		)
	])
	
	dnl | Doxygen
	dnl +---------
	AS_IF([test "x$DOXYGEN_ENABLE_DOC_ANY" != "xno"],
	[
		AC_PATH_PROGS([DOXYGEN], [doxygen], [not found])
		AS_IF([test "x$DOXYGEN" = "xnot found"], [
			AS_IF([test "x$DOXYGEN_ENABLE_DOC_ANY" = "xYES"], [
				AC_MSG_ERROR([Doxygen repuired but not found in PATH ($PATH). (any of the --enable-doc-* options)])
			], [
				DOXYGEN_enable_doc_man=no
				DOXYGEN_ENABLE_DOC_MAN=NO
				DOXYGEN_enable_doc_html=no
				DOXYGEN_ENABLE_DOC_HTML=NO
				DOXYGEN_ENABLE_DOC_ANY=NO
				DOXYGEN_ENABLE_DOC_ALL=NO
			])
		], [
			AS_IF([test "x$DOXYGEN_enable_doc_man" != "xno"], [
				DOXYGEN_enable_doc_man=yes
				DOXYGEN_ENABLE_DOC_MAN=YES
				DOXYGEN_ENABLE_DOC_ANY=YES
			], [
				DOXYGEN_ENABLE_DOC_ALL=NO
			])
			AS_IF([test "x$DOXYGEN_enable_doc_html" != "xno"], [
				DOXYGEN_enable_doc_html=yes
				DOXYGEN_ENABLE_DOC_HTML=YES
				DOXYGEN_ENABLE_DOC_ANY=YES
			], [
				DOXYGEN_ENABLE_DOC_ALL=NO
			])
		])
	])
	
	dnl | ENABLE_DOC_ALL
	dnl +----------------
	AM_CONDITIONAL([DOXYGEN_ENABLE_DOC_ALL_AM], [test "x$DOXYGEN_ENABLE_DOC_ALL" = "xYES"])
	
	dnl | ENABLE_DOC_ANY
	dnl +----------------
	AM_CONDITIONAL([DOXYGEN_ENABLE_DOC_ANY_AM], [test "x$DOXYGEN_ENABLE_DOC_ANY" = "xYES"])
	
	dnl | ENABLE_DOC_HTML
	dnl +----------------
	AM_CONDITIONAL([DOXYGEN_ENABLE_DOC_HTML_AM], [test "x$DOXYGEN_enable_doc_html" = "xyes"])
	
	dnl | ENABLE_DOC_MAN
	dnl +----------------
	AM_CONDITIONAL([DOXYGEN_ENABLE_DOC_MAN_AM], [test "x$DOXYGEN_enable_doc_man" = "xyes"])
])dnl AX_TEMPLATE_DOXYGEN
