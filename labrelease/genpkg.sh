#!/bin/sh

usage() {
	cat << EOF
Usage: $self <name> <desc> <repo> <version> <license> <depends>

Generate a package for the current linux distribution.
EOF
}

if test "$#" -lt 5; then
	usage
	exit 1
fi

name="$1"
desc="$2"
repo="$3"
version="$4"
license="$5"
depends="$6"
file="$(echo *.tar*)"

echo 'Generating package for os-release:'
cat /etc/os-release
echo ''

checkpkgman() {
	cmd="$1"
	shift
	for pm; do
		echo "Checking for $pm ..." | tr -d '\n'
		if which "$pm" &> /dev/null; then
			# Found
			echo ' yes'
			"$cmd"
			break
		else
			echo ' no'
		fi
	done
}

pkg_dpkg() {
	echo 'Packaging for dpkg ...'
	tar xvf "$file"
	cd "$dir"
	./configure
	make
	make DESTDIR="$(pwd)/../dpkginst"
	cd ../dpkginst
	mkdir -p DEBIAN
	cat > DEBIAN/control << EOF
Package: $name
Version: $version-0
Section: libs
Priority: extra
Architecture: i386 amd64
Depends: $depends
Installed-Size: $(du --exclude='DEBIAN/*' --block-size=K . | tail -n1 | sed 's/K.*$//')
Maintainer: automatic genpkg
Homepage: $repo
Description: $desc
EOF
	cd ..
	dpkg -b ./dpkginst "$name.deb"
}

pkg_rpm() {
	echo 'Imagine packaging for rpm ...'
}

pkg_pacman() {
	echo 'Packaging for pacman ...'
	dir="${file%.tar*}"
	cat > PKGBUILD << EOF
# Maintainer: Automatic
pkgname='$name'
pkgver='${version#v}'
pkgrel=1
epoch=
pkgdesc='$desc'
arch=(x86_64 i868)
url='$repo'
license=('$license')
groups=()
depends=($depends)
makedepends=(gcc make sed)
checkdepends=(make)
optdepends=()
provides=()
conflicts=()
replaces=()
backup=()
options=()
install=
changelog=
source=('$file')
noextract=()
md5sums=('SKIP')
validpgpkeys=()

prepare() {
	cd "$dir"
	./configure --prefix=/usr
}

build() {
	cd "$dir"
	make
}

check() {
	cd "$dir"
	make -k check
}

package() {
	cd "$dir"
	make DESTDIR="\$pkgdir" install
}
EOF
	makepkg -s
}

checkpkgman pkg_dpkg dpkg
checkpkgman pkg_rpm rpm
checkpkgman pkg_pacman pacman
