#!/bin/sh

self="$0"

usage() {
	cat << EOF
Usage: $self <path to NEWS> <path to description> <version>

Extract the news of <version> from <NEWS> and save them in <description>.
EOF
}

if test "$#" -lt 3; then
	usage
	exit 1
fi

from="$1"
to="$2"
version="$3"

isGoodContext() {
	# Exit with != 0 if contains part of another release
	if tail -n-1 | grep -q '# '; then
		return 1
	else
		return 0
	fi
}


if grep -q "^# $version" "$from"; then
	# Contains version!
	context=1
	while grep "^# $version" --after-context="$context" "$from" | isGoodContext; do
		context="$(( "$context" + 1 ))"
		if test "$context" -gt "$(grep "^# $version" --after-context="$context" "$from" | wc --lines)"; then
			# Case of first release: Break after some lines
			break
		fi
	done
	grep "^# $version" --after-context="$(( "$context" - 1 ))" "$from" | tail -n+3 > "$to"
	echo "Release notes found for version $version:"
	cat "$to"
	exit
else
	exit 2
fi
