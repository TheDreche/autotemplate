# autotemplate

## Description

Several autotools/GitLab templates for some needs. Specifically:
- autodep: Automatic dependency management
- autofeature: Add optional features easily
- doxygen: Generate documentation using [doxygen](https://www.doxygen.nl)
- labrelease: Utilities for releasing the project
- verinfo: Commit and version information even availible after make dist

## Usage

For using, just add a submodule to the repository in which you want to use it. If using gitlab CI, make sure to have th relative path in `.gitmodules` and to set the `GIT_SUBMODULE_STRATEGY` CI variable to either `normal` or `recursive`.

For usage of any of the above modules, the corresponding directory will have a `README.md` explaining the usage.
The file `./shellverbose.mk` is special: It contains information on usage inside itself. Use it for working with the shell in `Makefile.am`.

## Roadmap

First, adding all of the parts mentioned above. However, that list may expand over time.
