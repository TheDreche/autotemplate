#!/usr/bin/automake
# 
# Include this file for executing shell scripts in your `Makefile.am`
# 
# Your rules using the shell should then look like something like this:
# 
# 	$(shell_verbose)$(SHELL) -- $<
# 

shell_verbose = $(shell_verbose_@AM_V@)
shell_verbose_ = $(shell_verbose_@AM_DEFAULT_V@)
shell_verbose_0 = @echo "  SH      " $<;

# vim:filetype=automake:
