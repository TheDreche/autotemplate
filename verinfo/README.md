# Verinfo interface

Use this if you want to keep track of the version of some files (specifically, their commit), even if the source is extracted from a distribution tarball.

## Requirements

- automake

## Usage

### General setup

1.	In your `Makefile.am` the following variables have to be initialized:
	- `EXTRA_DIST`
	- `dist_noinst_SCRIPTS`
2.	If not already done, include `shellverbose.mk` (found in the root directory of this repository) into `Makefile.am`:
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~make
	include ./templates/shellverbose.mk
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
3.	In your `Makefile.am`, set the variables `VERINFO_DIRS`, `VERINFO_FILES` and `VERINFO_FILE`. There actually is a great difference:
	- `VERINFO_FILE` is the file used to record the version
	- `VERINFO_FILES` are the files to record the version from
	- `VERINFO_DIRS` are the directorys to record the version from
4.	Set `VERINFO_COMMITOF` to the path to the `commitof.sh` script
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~make
	VERINFO_COMMITOF = $(srcdir)/templates/verinfo/commitof.sh
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
5.	In your `Makefile.am`, `include` the file called `verinfo.mk`.

That setup should look similar to this:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~make
VERINFO_FILE = verinfo
VERINFO_FILES = configure.ac
include templates/verinfo/verinfo.mk
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In that example, the version of `configure.ac` is being kept track of in `verinfo`. That file will only exist once `make dist` has been called. You can explicitly generate it by using the equally called Makefile target (`make verinfo`).

In case you want to get the version of a file, use the `commitof.sh` script provided in this repository. See its `--help` output for more information.

### Autoconf

If you want to embed the commit that last changed `configure.ac` into `configure`, embed this in your `configure.ac` and adjust the paths (`commitof.sh` and `$(VERINFO_FILE)` as last `verinfo` in `--verinfo=verinfo`):

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~m4
AC_REVISION([m4_esyscmd([./templates/verinfo/commitof.sh --verinfo=verinfo --fallback=unknown --modified=+dirty --newline=no -- configure.ac])])
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For being able to get that information even without git, you will have to record the version. Add this to your `Makefile.am`:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~make
VERINFO_FILES += configure.ac
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Doxygen

You can integrate this into doxygen. This assumes you also use the equally named module of autotemplates.
First, put this in your `Doxyfile.in`:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Doxyfile
PROJECT_NUMBER = @DOXYFILE_VERSION@
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For substituting `@DOXYFILE_VERSION@`, you will have to put this into your `configure.ac` and adjust the path to `commitof.sh`:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~m4
AC_SUBST(DOXYFILE_VERSION)
DOXYFILE_VERSION="$(./autotemplates/verinfo/commitof.sh --fallback=unknown --modified=+dirty -- Doxyfile.in)"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For being able to access that information without the git repository being availible, you will have to record the version. Put this into your `Makefile.am`:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~make
VERINFO_FILES += Doxyfile.in
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Deduplicating paths

And a tip: If you want to mention the path to `commitof.sh` as few times as possible, `AC_SUBST`itute the path to all files:

Remove the following from `Makefile.am`:
~~~~~~~~~~~~~~~~~~~~~~~~~~Makefile.am
VERINFO_COMMITOF = <...>
VERINFO_FILE = <...>
~~~~~~~~~~~~~~~~~~~~~~~~~~

Insert the following in your configure.ac (adjust the path):
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~m4
m4_define([VERINFO_COMMITOF_PATH], [./templates/verinfo/commitof.sh])
m4_define([VERINFO_FILE_PATH], [verinfo])
# For autoconf AC_REVISION:
AC_REVISION([m4_esyscmd([VERINFO_COMMITOF_PATH --verinfo=VERINFO_FILE_PATH --modified=+dirty --fallback=unknown -- configure.ac])])
# For Makefile.am
AC_SUBST(VERINFO_FILE)
AC_SUBST(VERINFO_COMMITOF)
VERINFO_FILE=VERINFO_FILE_PATH
VERINFO_COMMITOF=VERINFO_COMMITOF_PATH
# For doxygen
AC_SUBST(DOXYFILE_VERSION)
DOXYFILE_VERSION="$($(VERINFO_COMMITOF) --verinfo=$(VERINFO_FILE) --modified=+dirty --fallback=unknown -- Doxyfile.in)"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Fun fact

If you read this whole `README.md` about twice you have read about as many lines as the sources for this module have.
