#!/usr/bin/automake

EXTRA_DIST += $(VERINFO_FILE)
dist_noinst_SCRIPTS += $(VERINFO_COMMITOF)

$(srcdir)/$(VERINFO_FILE) : $(VERINFO_COMMITOF) $(VERINFO_FILES)
	$(shell_verbose) $(SHELL) $(VERINFO_COMMITOF) --verinfo='$(VERINFO_FILE)' --update -- $(VERINFO_FILES) $(VERINFO_DIRS) > /dev/null || touch "$(VERINFO_FILE)"

# vim:filetype=automake:
